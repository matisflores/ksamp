# Ksamp

**Laboratorio 1: Observando el Comportamiento de Linux**

*Objetivos*

Iniciar los laboratorios con un proyecto sencillo, donde se muestre como a través del
sistema de archivos virtual `/proc` de Linux, podemos inspeccionar información interna del
kernel. Se deberá generar la utilidad **ksamp** que muestra de diversas formas algún
subconjunto de la información disponible en `/proc`.

*Introducción*

Podemos pensar el kernel de Linux como una colección de funciones y estructuras de
datos. Estas estructuras de datos (o variables de kernel) contienen la visión del kernel
respecto al estado del sistema, donde cada interrupción, cada llamada al sistema, cada fallo
de protección hacen que este estado cambie. Inspeccionando las variables del kernel podemos obtener información relevante de los procesos, interrupciones, dispositivos, sistemas de archivos, capacidades del hardware, etc.

Muchas variables conforman el estado del kernel y estas pueden estar alojadas de manera estática o dinámica en un stack frame de la memoria del kernel. Dado que el kernel de Linux es un programa "C" de código abierto , es posible inspeccionar el código fuente y encontrar algunos ejemplos relevantes de estas variables. Por ejemplo, en **kernels 2.4.x**, `xtime` definido en *include/linux/sched.h* que mantiene la hora del sistema, la estructura `task_struct` definida en *include/linux/sched.h* que contiene la descripción completa de un proceso, o los valores `nr_threads` y `nr_running` definidos en *kernel/fork.c* los cuales indican cuantos procesos existen y cuántos de estos están corriendo.
El código fuente de Linux lo podemos encontrar en el directorio */usr/src/linux* de la mayoría de las distribuciones, pero también existen páginas donde podemos navegar el código de varias versiones de kernel, como por ejemplo *Linux Cross Reference*. También existen utilidades para navegación de código local como *Source Navigator*.
