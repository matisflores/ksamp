#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#define SIZE 256

void
getKeyFromProcFile (char* file, char* value, char* key);
void
formatSeconds (float secs, char* buf);
void
header ();
void
hostname ();
void
fecha ();
void
parteB ();
void
CPU ();
void
kernel ();
void
uptime ();
void
filesystems ();
void
parteC ();
void
cputimes ();
void
contexts ();
void
bootTime ();
void
processes ();
void
parteD (int interval, int duration);
void
HDD ();
void
memmory ();
void
load ();

/**
 * Obtener un valor desde archivo en /proc
 * @param file - archivo de busqueda
 * @param value - resultado
 * @param Key - a buscar.
 */
void
getKeyFromProcFile (char* file, char* value, char* key)
{
  char buffer[500];
  char* match = NULL;
  FILE* fd;
  fd = fopen (file, "r");

  while (feof (fd) == 0)
    {
      fgets (buffer, 500, fd);
      match = strstr (buffer, key);
      if (match != NULL)
	break;
    }

  fclose (fd);
  strcpy (value, match);
  return;
}

/**
 * Segundos a formato DD HH:MM:SS
 * @param secs
 * @param buf Tiempo en formato DD HH:MM:SS
 */
void
formatSeconds (float secs, char* buf)
{
  unsigned int d, h, m;
  float s;

  d = (int) (secs / 86400);
  secs = secs - (long) (d * 86400);
  h = (int) (secs / 3600);
  secs = secs - (long) (h * 3600);
  m = (int) (secs / 60);
  secs = secs - (long) (m * 60);
  s = secs;
  if (d > 0)
    sprintf (buf, "%3ud %2u:%02u:%02.2f\n", d, h, m, secs);
  else
    sprintf (buf, "%2u:%02u:%02.2f\n", h, m, s);
  return;
}

/**
 * Cabecera
 */
void
header ()
{
  printf ("-----------------------------------------\n");
  hostname ();
  fecha ();
  printf ("-----------------------------------------\n");
  return;
}

/**
 * Nombre de pc
 */
void
hostname ()
{
  char name[30];
  FILE *fd;
  fd = fopen ("/proc/sys/kernel/hostname", "r");

  fscanf (fd, "%30[^\n]s", name);
  printf ("Hostname: %s \n", name);
  fclose (fd);
  return;
}

/**
 * Fecha y hora actuales.
 */
void
fecha ()
{
  char fecha[12];
  char value[SIZE + 1];
  char hora[13];
  getKeyFromProcFile ("/proc/driver/rtc", value, "rtc_date");
  sscanf (value, "rtc_date : %11s", fecha);
  getKeyFromProcFile ("/proc/driver/rtc", value, "rtc_time");
  sscanf (value, "rtc_time : %12s", hora);
  printf ("Fecha: %s   -   Hora: %s \n", fecha, hora);
  return;
}

/**
 * Consigna B
 */
void
parteB ()
{
  CPU ();
  kernel ();
  uptime ();
  filesystems ();
  return;
}

/**
 * CPU info
 */
void
CPU ()
{
  char modelo[100];
  char value[SIZE];
  char tipo[50];

  getKeyFromProcFile ("/proc/cpuinfo", value, "vendor_id");
  sscanf (value, "vendor_id : %49s", tipo);

  getKeyFromProcFile ("/proc/cpuinfo", value, "model name");
  sscanf (value, "model name : %100[^\n]c", modelo);

  printf ("\nCPU - Tipo: %s    Modelo: %s\n", tipo, modelo);
  return;
}

/**
 * Kernel info
 */
void
kernel ()
{
  FILE *fd;
  char version[60];
  fd = fopen ("/proc/version", "r");
  fscanf (fd, "%60[^(]s", version);
  printf ("Kernel: %s \n", version);
  fclose (fd);
  return;
}

/**
 * Uptime
 */
void
uptime ()
{
  FILE *fd;
  float time;
  char hms[18];
  fd = fopen ("/proc/uptime", "r");
  fscanf (fd, "%f", &time);
  formatSeconds (time, hms);
  printf ("Uptime: %s", hms);
  fclose (fd);
  return;
}

/**
 * FileSystem soportados
 */
void
filesystems ()
{
  FILE* fd = fopen ("/proc/filesystems", "r");
  int ch, lines = 0;

  do
    {
      ch = fgetc (fd);
      if (ch == '\n')
	lines++;
    }
  while (feof (fd) == 0);

  fclose (fd);

  printf ("Sistemas de Archivo soportados: %d\n", lines);
}

/**
 * Consigna C
 */
void
parteC ()
{
  cputimes ();
  contexts ();
  bootTime ();
  processes ();
  return;
}

/**
 * tiempos de cpu de usuario, sistema, e idle.
 */
void
cputimes ()
{
  FILE *fd;
  float user, sys, idle;
  char tUser[15], tSys[15], tIdle[15];
  fd = fopen ("/proc/stat", "r");
  fscanf (fd, "cpu %f %*f %f %f", &user, &sys, &idle);
  formatSeconds (user / 100, tUser);
  printf ("Tiempo de cpu (usuario): %s", tUser);
  formatSeconds (sys / 100, tSys);
  printf ("Tiempo de cpu (sistema): %s", tSys);
  formatSeconds (idle / 100, tIdle);
  printf ("Tiempo de cpu (idle): %s", tIdle);
  fclose (fd);
  return;
}

/**
 * context info
 */
void
contexts ()
{
  char value[256];
  unsigned int cambios;

  getKeyFromProcFile ("/proc/stat", value, "ctxt");
  sscanf (value, "ctxt %u", &cambios);

  printf ("Cambios de contexto: %u \n", cambios);
  return;
}

/**
 * Fecha y hora de inicio
 */
void
bootTime ()
{
  char value[SIZE];
  time_t btime;
  unsigned int aux;
  char booted[40];

  getKeyFromProcFile ("/proc/stat", value, "btime");
  sscanf (value, "btime %u", &aux);
  btime = (time_t) aux;

  strftime (booted, sizeof(booted), "%c", localtime (&btime));
  printf ("Boot Time: %s \n", booted);
  return;
}

/**
 * Procesos creados desde el inicio
 */
void
processes ()
{
  char value[SIZE];
  unsigned int procesos;

  getKeyFromProcFile ("/proc/stat", value, "processes");
  sscanf (value, "processes %u", &procesos);

  printf ("Procesos creados: %u\n", procesos);
  return;
}

/**
 * Consigna D
 * @param interval
 * @param duration
 */
void
parteD (int interval, int duration)
{
  int i;
  i = duration / interval;
  printf ("+++++++++++++++++++++++++++++++++++++++++\n");
  while (i > 0)
    {
      HDD ();
      memmory ();
      load ();
      i--;
      sleep (interval);
      printf ("+++++++++++++++++++++++++++++++++++++++++\n");
    }
  return;
}

/**
 * Pedidos a disco
 */
void
HDD ()
{
  char value[SIZE];
  unsigned int lecturas, escrituras, pedidos;

  getKeyFromProcFile ("/proc/diskstats", value, "sda");
  sscanf (value, "sda %u", &lecturas);
  sscanf (value, "sda %*u %*u %*u %*u %u", &escrituras);
  pedidos = escrituras + lecturas;
  printf ("Pedidos al disco: %u\n", pedidos);
  return;
}

/**
 * Mem info
 */
void
memmory ()
{
  char value[SIZE];
  unsigned int memTotal, memFree;

  getKeyFromProcFile ("/proc/meminfo", value, "MemTotal");
  sscanf (value, "MemTotal: %u", &memTotal);
  getKeyFromProcFile ("/proc/meminfo", value, "MemFree");
  sscanf (value, "MemFree: %u", &memFree);
  printf ("Mem total: %u MB\n", memTotal / 1024);
  printf ("Mem free: %u MB\n", memFree / 1024);
  return;
}

/**
 * Promedios de carga de un minuto
 */
void
load ()
{
  FILE *fd;
  float load;
  fd = fopen ("/proc/loadavg", "r");
  fscanf (fd, "%f", &load);
  printf ("Promedio de carga de un minuto: %f\n", load);
  fclose (fd);
  return;
}

#endif /* FUNCTIONS_H_ */
