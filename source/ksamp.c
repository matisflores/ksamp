#include "functions.h"

int
main (int argc, char **argv)
{
  unsigned int a, b, c;
  short int flagC = 0;
  short int flagD = 0;

  while ((c = getopt (argc, argv, "sl:")) != -1)
    {
      switch (c)
	{
	case 's':
	  if (argc == 2)
	    {
	      flagC = 1;
	      break;
	    }
	  return 0;
	case 'l':
	  if (argc == 4)
	    {
	      flagC = 1;
	      flagD = 1;
	      break;
	    }
	  return 0;
	default:
	  printf ("Options: -s , -l interval duration\n");
	  return 0;
	}
    }

  header ();
  parteB ();

  if (flagC)
    parteC ();
  if (flagD)
    {
      sscanf (argv[2], "%u", &a);
      sscanf (argv[3], "%u", &b);
      parteC ();
      parteD (a, b);
    }
  return 0;
}
